package org.game;

public class Main {
    public static void main(String[] args) {
        Simulation simulation = new Simulation(25, 25);
        simulation.setAlive(11, 11);
        simulation.setAlive(12, 12);
        simulation.setAlive(10, 13);
        simulation.setAlive(11, 13);
        simulation.setAlive(12, 13);
        for (int i = 0; i < 5; i++) {
            simulation.printBoard();
            simulation.newGeneration();
        }
    }
}